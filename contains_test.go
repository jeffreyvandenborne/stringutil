package stringutil

import "testing"

func TestContainsString(t *testing.T) {
	testSlice := []string{
		"foobar",
		"something else",
		"narwhals",
	}

	if contains := ContainsString("narwhals", testSlice); !contains {
		t.Errorf("narwhals should be contained in slice: %v", testSlice)
	}

	if contains := ContainsString("John doe", testSlice); contains {
		t.Errorf("John doe should not be in slice: %v", testSlice)
	}
}
