package stringutil

func ContainsString(key string, search []string) bool {
	for _, currentVal := range search {
		if currentVal == key {
			return true
		}
	}

	return false
}
