package ignorecase

import "strings"

func ContainsString(key string, search []string) bool {
	lowercaseCaseKey := strings.ToLower(key)
	for _, currentVal := range search {
		if strings.ToLower(currentVal) == lowercaseCaseKey {
			return true
		}
	}

	return false
}
